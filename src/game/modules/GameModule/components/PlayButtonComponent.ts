import {IComponent} from "../../../../engine/components/IComponent";
import {GameObject} from "../../../../engine/gameobject/GameObject";
import {CStateMachine} from "../../../../engine/components/CStateMachine";
import {DI} from "../../../../engine/DI";
import {ResponseParser} from "../models/ResponseParser";
import {GameModel} from "../models/GameModel";
import EventEmitter = PIXI.utils.EventEmitter;

export class PlayBtnComponent implements IComponent
{
    static ID: string = "PlayBtnComponent";
    static START_SPIN = "START_SPIN";
    static SET_STATE = "play_button_state";
    static ENABLED = "enabled";
    static DISABLED = "disabled";
    gameObject: GameObject;
    stateMachine: CStateMachine;

    emitter: EventEmitter = DI.Inject(EventEmitter);
    view: PIXI.DisplayObject;

    responseParser: ResponseParser = DI.Inject(ResponseParser);
    gameModel: GameModel = DI.Inject(GameModel);

    start()
    {
        this.stateMachine = this.gameObject.compDict[CStateMachine.ID];

        this.view = this.gameObject.findComponentByKey("visible");
        this.view.interactive = true;
        this.view.on("pointerdown", this.startSpin, this);
        this.emitter.on(PlayBtnComponent.SET_STATE, this.updateState, this);
        this.updateState(PlayBtnComponent.ENABLED);
    }

    startSpin()
    {
        if (this.stateMachine.lastState === PlayBtnComponent.ENABLED && this.responseParser.balance >= this.gameModel.bet) {
            this.emitter.emit(PlayBtnComponent.START_SPIN);
        }
    }

    updateState(state: string)
    {
        this.stateMachine.setState(state);
    }

    stop()
    {
        this.view.removeListener("pointerdown", this.startSpin);
        this.emitter.off(PlayBtnComponent.SET_STATE, this.updateState, this);
    }

}