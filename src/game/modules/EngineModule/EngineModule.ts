import {IModule} from "../../../engine/Module";
import {Engine2D} from "../../../engine/Engine2D";

export class EngineModule implements IModule
{
    async init()
    {
        const [prefabsData, imagesData, resourceData] = await Promise.all([
            this.getJSON("../../../../resources/prefabs.json"),
            this.getJSON('../../../../resources/images.json'),
            this.getJSON('../../../../resources/groups.json')]);
        Engine2D.it.init({
            parentHTML: document.body,
            screenSizeRangeX: [700*2, 1136*2],
            screenSizeRangeY: [400*2, 768*2],
            bgColor: 0x222222,
            prefabsData,
            imagesData,
            resourceData
        });
    }

    async getJSON(path)
    {
        const result = await fetch(path);
        return result.json();
    }

    run()
    {

    }
}