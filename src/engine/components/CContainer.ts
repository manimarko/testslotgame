import { GameObject } from "../gameobject/GameObject";
import { IComponent } from "./IComponent";
import { DisplayObject } from "pixi.js";

export class CContainer extends PIXI.Container implements IComponent {
    static ID:string = "cont";
    gameObject: GameObject;

    onChildAdded(child: GameObject) {
        const displayObj = this.getDisplayObject(child);
        if (displayObj) {
            this.addChild(displayObj);
        }
    }

    onChildRemoved(child: GameObject) {
        const displayObj = this.getDisplayObject(child);
        if (displayObj) {
            this.removeChild(displayObj);
        }
    }

    getDisplayObject(child: GameObject):PIXI.DisplayObject {
        return child.findComponentByKey('visible');
    }

    start() {
        this.name = this.gameObject.name;
        this.gameObject.children.forEach(child => {
            const displayObj = this.getDisplayObject(child);
            if (displayObj) {
                this.addChild(displayObj);
            }
        });
    }

    stop() {

    }
}
