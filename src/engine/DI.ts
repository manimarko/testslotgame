export type AConstructorTypeOf<T> = new (...args: any[]) => T;

export class DI
{
    static map: Map<AConstructorTypeOf<any>, any> = new Map();

    static Inject<C>(obj: AConstructorTypeOf<C>): C
    {
        const info = DI.map.get(obj);
        if (info.singleton) {
            if (!info.instance) {
                info.instance = new info.classSource();
            }
            return info.instance;
        }
        return new info.classSource();
    }

    static Bind(obj, options: { singleton?: boolean, classSource?: any, instance?: any } = {})
    {
        DI.map.set(obj, {singleton: false, classSource: obj, instance: null, ...options});
    }
}