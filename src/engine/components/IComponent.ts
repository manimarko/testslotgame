import { GameObject } from "../gameobject/GameObject";
export interface IComponent {
    gameObject: GameObject;
}
