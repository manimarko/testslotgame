export interface IModule
{
    init: () => Promise<void>
    run?: () => void
}