import {IModule} from "../../../engine/Module";
import {DI} from "../../../engine/DI";
import {ServerAPI} from "./models/ServerAPI";
import {ResponseParser} from "./models/ResponseParser";
import {BalanceComponent} from "./components/BalanceComponent";
import {ReelsModel} from "./models/ReelsModel";
import {GameObjectBuilder} from "../../../engine/gameobject/GameObjectBuilder";
import {PlayBtnComponent} from "./components/PlayButtonComponent";
import {ReelComponent} from "./components/ReelComponent";
import {WinAmountComponent} from "./components/WinAmountComponent";
import {MultiItemsPool} from "../../../engine/Pool";
import {ActionNodesBuilder} from "../../../engine/command/ActionNodesBuilder";
import {ReelModel} from "./models/ReelModel";
import {PayLinesModel} from "./models/PayLinesModel";
import {GameObject} from "../../../engine/gameobject/GameObject";
import {CContainer} from "../../../engine/components/CContainer";
import {CAnchor} from "../../../engine/components/CAnchor";
import {InitGameAction} from "./actions/InitGameAction";
import {IdleGameAction} from "./actions/IdleGameAction";
import {MakeSpinAction} from "./actions/MakeSpinAction";
import {CanStopSpinningAction} from "./actions/CanStopSpinningAction";
import {WinningsAction} from "./actions/WinningsAction";
import {GameModel} from "./models/GameModel";
import {ServerEmulator} from "./models/ServerEmulator";
import {PayTableComponent} from "./components/PayTableComponent";
import EventEmitter = PIXI.utils.EventEmitter;

export class GameModule implements IModule
{
    /**
     * configure dependencies
     */
    initializeDI()
    {
        DI.Bind(EventEmitter, {singleton: true});
        DI.Bind(ReelModel);
        DI.Bind(GameModel, {singleton: true});
        DI.Bind(ReelsModel, {singleton: true});
        DI.Bind(ServerAPI, {classSource: ServerEmulator, singleton: true});
        DI.Bind(MultiItemsPool, {singleton: true});
        DI.Bind(ResponseParser, {singleton: true});
        DI.Bind(ActionNodesBuilder, {singleton: true});
        DI.Bind(PayLinesModel, {singleton: true});
    }

    /**
     * Add game specific components
     */
    initializeRules()
    {
        GameObjectBuilder.it.addComponents([PlayBtnComponent, BalanceComponent, ReelComponent, WinAmountComponent, PayTableComponent]);

        GameObjectBuilder.it.addRule((go: GameObject) => !go.findComponentByKey("visible"),
            (go: GameObject) => go.addComponent(CContainer));

        GameObjectBuilder.it.addRule((go: GameObject) =>
            {
                return go.parent && go.parent.compDict[CAnchor.ID] && !go.compDict[CAnchor.ID]
            },
            (go: GameObject) => go.addComponent(CAnchor));
    }

    /**
     * Here node tree is described
     */
    makeGameFlowAndStart()
    {
        const nodesBuilder = DI.Inject(ActionNodesBuilder);
        nodesBuilder.get("Root.InitGame").setAction(InitGameAction);

        nodesBuilder.get("Root.GameFlow").setArgs({looped: true});

        nodesBuilder.get("Root.GameFlow.Idle").setAction(IdleGameAction);
        nodesBuilder.get("Root.GameFlow.MakeSpin").setAction(MakeSpinAction);
        nodesBuilder.get("Root.GameFlow.CanStopSpinning").setAction(CanStopSpinningAction);
        nodesBuilder.get("Root.GameFlow.Winning").setAction(WinningsAction);

        nodesBuilder.rootNode.execute();
    }

    init()
    {
        this.initializeDI();
        this.initializeRules();
    }

    run()
    {
        this.makeGameFlowAndStart();
    }
}