import {DI} from "../../../../engine/DI";
import {ReelModel, ReelState} from "./ReelModel";

export class ReelsModel
{
    reels: ReelModel[] = [DI.Inject(ReelModel), DI.Inject(ReelModel), DI.Inject(ReelModel)];
    stopPromise: Promise<any> = Promise.resolve();

    constructor()
    {
        this.reels.forEach((reel, i) =>
        {
            reel.id = i;
        });
    }

    init(reelsSet, initialView): void
    {
        this.reels.forEach((reel, i) =>
        {
            reel.init(reelsSet[i], initialView[i])
        });
    }

    start(): void
    {
        this.reels.forEach((reel, i) =>
        {
            reel.start();
            reel.startTime = -i * 0.1;
        });
        this.stopPromise = Promise.all(this.reels.map(reel => reel.stopPromise));
    }

    canStop(view): void
    {
        this.reels.forEach((reel, i) =>
        {
            reel.canStop([...view[i]]);
            reel.stateTime += -0.5 * i;
        });
    }

    stop(): void
    {
        this.reels.forEach((reel, i) =>
        {
            reel.stop();
        });
    }

    get canStopped(): boolean
    {
        return this.reels.every(reel => reel.canStopped);
    }

    get commonState(): ReelState | undefined
    {
        if (this.reels.some((reel, i) => i > 0 && reel.state !== this.reels[i - 1].state)) {
            //if not all reels have the same commonState
            return undefined;
        }
        return this.reels[0].state;
    }
}