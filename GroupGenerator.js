const path = require('path'), fs = require('fs');

function saveObject(path, data) {
    fs.writeFileSync(path, JSON.stringify(data, null, '\t'));
}

const destFolder = './dist/resources/';
const ungrouped = "main";

fs.mkdirSync(destFolder,{recursive:true});

const imagesData = JSON.parse(fs.readFileSync(destFolder + 'images.json', "utf-8"));

let groupsFilePath = destFolder + 'groups.json';

let groupsData = {};

if (fs.existsSync(groupsFilePath)) {
    groupsData = JSON.parse(fs.readFileSync(groupsFilePath, "utf-8"));
}

if (!groupsData[ungrouped]) {
    groupsData[ungrouped] = [];
}

const groupNames = Object.keys(groupsData);

for (const prefabName in imagesData) {
    const isExistAnywhere = groupNames.some(groupName => {
        return groupsData[groupName].some((item) => item === prefabName);
    });

    if (!isExistAnywhere) {
        groupsData[ungrouped].push(prefabName);
    }
}

saveObject(groupsFilePath, groupsData);

console.log("groups generated");