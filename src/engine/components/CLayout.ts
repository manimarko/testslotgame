import {CPivot} from './CPivot';
import {CAnchor} from './CAnchor';
import {GameObject} from "../gameobject/GameObject";
import {IComponent} from "./IComponent";
import {CAutoFit} from './CAutoFit';

export class CLayout implements IComponent {
    static ID: string = "layout";
    gameObject: GameObject;
    _delta: number = 10;
    _layout: string = "h";

    get layout(): string {
        return this._layout;
    }

    set layout(v: string) {
        this._layout = v;
    }

    get delta(): number {
        return this._delta;
    }

    set delta(v: number) {
        this._delta = v;
    }

    onChildAdded(child: GameObject) {
        this.updateChildrenPositions();
    }

    onChildRemoved(child: GameObject) {
        this.updateChildrenPositions();
    }

    start() {
        //setInterval(this.updateChildrenPositions.bind(this), 10);
        this.gameObject.children.forEach(element => {
            this.onChildAdded(element);
        });
    }

    stop() {
    }

    updateChildrenPositions() {
        let v = 0;
        let comp: CAnchor = null;
        this.gameObject.children.forEach(child => {
            comp = child.compDict[CAnchor.ID];
            if (comp) {
                if (this._layout == "h") {
                    comp.left = v;
                    v += comp.xAnchors.size.calculatedValue + this.delta;
                }
                if (this._layout == "v") {
                    comp.top = v;
                    v += comp.yAnchors.size.calculatedValue + this.delta;
                }
            }
        });
    }

}
