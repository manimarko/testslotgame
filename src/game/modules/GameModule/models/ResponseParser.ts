import {DI} from "../../../../engine/DI";
import {ISpinData} from "./ServerAPI";
import EventEmitter = PIXI.utils.EventEmitter;

export class ResponseParser
{
    static RESPONSE_PARSED = "RESPONSE_PARSED";
    balance: number;
    winLines: any[];
    view: number[][];
    winAmount: number;
    emitter: EventEmitter = DI.Inject(EventEmitter);

    parse(data: ISpinData)
    {
        this.balance = data.balance;
        this.winLines = data.winLines;
        this.winAmount = data.winAmount;
        this.view = data.view;
        this.emitter.emit(ResponseParser.RESPONSE_PARSED);
        console.log("server response:" + JSON.stringify(data));
    }

}