import {DI} from "../../../../engine/DI";
import {ServerAPI} from "../models/ServerAPI";
import {ResponseParser} from "../models/ResponseParser";
import {IAction} from "../../../../engine/command/Node";
import {BalanceComponent} from "../components/BalanceComponent";
import {WinAmountComponent} from "../components/WinAmountComponent";
import {ReelComponent} from "../components/ReelComponent";
import {PlayBtnComponent} from "../components/PlayButtonComponent";
import {GameModel} from "../models/GameModel";
import EventEmitter = PIXI.utils.EventEmitter;

export class WinningsAction implements IAction
{
    emitter: EventEmitter = DI.Inject(EventEmitter);
    server: ServerAPI = DI.Inject(ServerAPI);
    responseParser: ResponseParser = DI.Inject(ResponseParser);
    gameModel: GameModel = DI.Inject(GameModel);

    async execute()
    {
        this.emitter.emit(BalanceComponent.UPDATE_BALANCE, this.responseParser.balance);
        if (this.responseParser.winLines.length > 0) {
            let winLineIndex = 0;
            this.emitter.emit(WinAmountComponent.UPDATE_WIN_AMOUNT, ` summary win ${this.responseParser.winAmount}, ${this.responseParser.winLines.length} winlines`);

            let intervalId;

            if (this.responseParser.winLines) {
                intervalId = setInterval(() =>
                {
                    if (winLineIndex >= this.responseParser.winLines.length) {
                        winLineIndex = 0;
                    }
                    const winLine = this.responseParser.winLines[winLineIndex];
                    this.emitter.emit(ReelComponent.SHOW_WIN_LINE, winLine);

                    //it can be done better, but for test task is ok
                    const descriptionsMap = {
                        "1": "3 CHERRY symbols on top line 2000",
                        "2": "3 CHERRY symbols on center line 1000",
                        "3": "3 CHERRY symbols on bottom line 4000",
                        "4": "3 7 symbols on any line 150",
                        "5": "Any combination of CHERRY and 7 on any line 75",
                        "6": "3 3xBAR symbols on any line 50",
                        "7": "3 2xBAR symbols on any line 20",
                        "8": "3 BAR symbols on any line 10",
                        "9": "Combination of any BAR symbols on any line give 5"
                    }
                    this.emitter.emit(WinAmountComponent.UPDATE_WIN_AMOUNT, `reward = ${winLine.reward} ${descriptionsMap[winLine.id]}`);
                    winLineIndex++;
                }, 1500);
            }

            let resolveForceSkip = null;
            const forceSkiPromise = new Promise((resolve) =>
            {
                resolveForceSkip = resolve;
            });
            const forceSkipped = () =>
            {
                resolveForceSkip();
                this.gameModel.isForceSpin = true;
            };
            this.emitter.addListener(PlayBtnComponent.START_SPIN, forceSkipped);
            await forceSkiPromise;
            clearInterval(intervalId);
            this.emitter.removeListener(PlayBtnComponent.START_SPIN, forceSkipped);

            this.emitter.emit(ReelComponent.SHOW_WIN_LINE);
            this.emitter.emit(WinAmountComponent.UPDATE_WIN_AMOUNT, " summary win = " + this.responseParser.winAmount);
        } else {
            this.emitter.emit(WinAmountComponent.UPDATE_WIN_AMOUNT, 0);
        }
    }
}