import {IComponent} from "../../../../engine/components/IComponent";
import {GameObject} from "../../../../engine/gameobject/GameObject";
import {DI} from "../../../../engine/DI";
import {CText} from "../../../../engine/components/CText";
import EventEmitter = PIXI.utils.EventEmitter;

export class WinAmountComponent implements IComponent {
    static UPDATE_WIN_AMOUNT = "UPDATE_WIN_AMOUNT";
    static ID: string = "WinAmountComponent";
    gameObject: GameObject;
    emitter: EventEmitter = DI.Inject(EventEmitter);
    textComponent: CText;

    start() {
        this.textComponent = this.gameObject.compDict[CText.ID];
        this.emitter.on(WinAmountComponent.UPDATE_WIN_AMOUNT, this.updateText, this);
    }

    updateText(amount): void {
        this.textComponent.textValue = amount;
    }

    stop() {
        this.emitter.removeListener(WinAmountComponent.UPDATE_WIN_AMOUNT, this.updateText);
    }
}