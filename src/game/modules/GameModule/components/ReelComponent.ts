import {IComponent} from "../../../../engine/components/IComponent";
import {GameObject} from "../../../../engine/gameobject/GameObject";
import {DI} from "../../../../engine/DI";
import {ReelModel} from "../models/ReelModel";
import {MultiItemsPool} from "../../../../engine/Pool";
import {Engine2D} from "../../../../engine/Engine2D";
import {ReelsModel} from "../models/ReelsModel";
import {IWinline} from "../models/ServerAPI";
import EventEmitter = PIXI.utils.EventEmitter;

export class ReelComponent implements IComponent
{
    static ID: string = "reel_component";
    static SHOW_WIN_LINE = "SHOW_WIN_LINE";
    SYMBOL_SIZE_X = 141;
    SYMBOL_SIZE_Y = 121;
    gameObject: GameObject;
    emitter: EventEmitter = DI.Inject(EventEmitter);
    model: ReelModel;
    pool: MultiItemsPool = DI.Inject(MultiItemsPool);
    view: PIXI.DisplayObject;
    winLine: IWinline;

    start()
    {
        this.model = DI.Inject(ReelsModel).reels[parseInt(this.gameObject.parent.name.replace('reel_', "")) - 1]
        Engine2D.it.pixiApp.ticker.add(this.update, this);
        this.view = this.gameObject.findComponentByKey("visible");
        const gr = new PIXI.Graphics();
        gr.beginFill(0, 1);
        gr.drawRect(0, 0, this.SYMBOL_SIZE_X, this.SYMBOL_SIZE_Y * 3);
        gr.endFill();
        this.view.mask = gr;
        this.emitter.on(ReelComponent.SHOW_WIN_LINE, this.showWinLine, this);
    }

    update(dt: number)
    {
        this.model.update(dt * 16 / 1000);
        if (!this.view.mask.parent && this.view.parent) {
            this.view.parent.addChild(this.view.mask);
            this.view.mask.x = this.view.x;
            this.view.mask.y = this.view.y;
        }
        this.gameObject.children.forEach(child => this.pool.back(child.name, child));
        this.gameObject.removeChildren();
        let position = -this.SYMBOL_SIZE_Y;
        for (let i = 0; i < this.model.view.length; i++) {
            let name = "symbol_" + this.model.view[i];
            let obj = this.pool.get(name);
            this.gameObject.addChild(obj);
            let comp = obj.findComponentByKey("visible");
            comp.y = position + this.model.symbolSpinningProgress * this.SYMBOL_SIZE_Y;
            position += this.SYMBOL_SIZE_Y;
            if (this.winLine && this.winLine.symbolPositions.some(symbolPosition => symbolPosition && symbolPosition[1] === this.model.id && symbolPosition[0] === i)) {
                comp.getChildByName("win_frame").visible = true;
            } else {
                comp.getChildByName("win_frame").visible = false;
            }
        }
    }

    showWinLine(winLine: IWinline): void
    {
        this.winLine = winLine;
    }

    stop()
    {
        this.emitter.removeListener(ReelComponent.SHOW_WIN_LINE, this.showWinLine);
        Engine2D.it.pixiApp.ticker.remove(this.update, this);
    }

}