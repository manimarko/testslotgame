import {GameObject} from "../gameobject/GameObject";
import {IComponent} from "./IComponent";

export class CText extends PIXI.Text implements IComponent {
    static ID: string = "CText";
    gameObject: GameObject;
    _font: string;
    _textValue: string;

    onChildAdded(child: GameObject) {

    }

    onChildRemoved(child: GameObject) {

    }

    start() {
        this.name = this.gameObject.name;
        this.style = new PIXI.TextStyle({fontSize: 20, fill: 0xFFFFFF})
    }

    set font(v: string) {
        this._font = v;
    }

    get textValue(): string {
        return this._textValue;
    }

    set textValue(v: string) {
        this._textValue = v;
        this.text = v;
        this.emit("text_changed");
    }

    get font(): string {
        return this._font;
    }

    stop() {

    }
}
