import EventEmitter = PIXI.utils.EventEmitter;
import {DI} from "../../../../engine/DI";

export class ReelModel
{
    SYMBOLS_PER_SECOND = 20;
    symbolSpinningProgress: number = 0;
    speed: number = 0;
    stack: number[] = [];
    view: number[] = [];
    _state: ReelState = ReelState.Stopped;
    stateTime: number = 0;
    reelSet: number[] = [];
    targetView: number[];
    emitter: EventEmitter = DI.Inject(EventEmitter);
    stopPromise: Promise<any> = Promise.resolve();
    stopPromiseResolve: Function;
    startTime: number = 0;
    id: number

    constructor()
    {
    }

    set state(v: ReelState)
    {
        if (v != this._state) {
            this._state = v;
            this.stateTime = 0;
        }
    }

    get state(): ReelState
    {
        return this._state;
    }

    init(reelSet, initialView): void
    {
        this.reelSet = [...reelSet];
        this.view = [...initialView];
    }

    update(dt: number)
    {
        this.stateTime += dt;
        if (this._state === ReelState.Spinning) {

            /**
             * some sort of cubic animation
             */
            this.startTime += dt;
            if (this.startTime > 0) {
                let t = Math.min(this.startTime * 2, 1);
                this.speed = Math.cos(Math.pow(t, 0.4) * Math.PI * 2) * t * this.SYMBOLS_PER_SECOND;
            }

            this.symbolSpinningProgress += dt * this.speed;
            if (this.stack.length === 0) {
                this.stack = [...this.reelSet];
            }
            if (this.stateTime >= 2 && this.targetView) {
                this.stop();
                return;
            }
        }

        if (this._state === ReelState.Stopping) {
            this.symbolSpinningProgress += dt * this.speed;
        }

        if (this._state === ReelState.Stopped) {

        }

        while (this.symbolSpinningProgress >= 1 && this.stack.length > 0) {
            this.symbolSpinningProgress--;
            if (this.view.length > 5) {
                this.view.pop();
            }
            this.view.unshift(this.stack.pop());
        }

        if (this.stack.length == 0 && this._state === ReelState.Stopping && this.symbolSpinningProgress >= 1) {
            this.state = ReelState.Stopped;
            this.symbolSpinningProgress = 1;
            this.stopPromiseResolve();
        }
    }

    start(): void
    {
        if (this._state === ReelState.Stopped) {
            this.state = ReelState.Spinning;
            this.speed = 0;
            this.startTime = 0;
            this.stopPromise = new Promise((resolve) =>
            {
                this.stopPromiseResolve = resolve;
            });
        }
    }

    canStop(view): void
    {
        this.targetView = view;
    }

    stop(): void
    {
        if (this.targetView) {
            this.state = ReelState.Stopping;
            this.stack.length = 0;
            this.stack = [...this.targetView, this.reelSet[0]];
            this.targetView = null;
        }
    }

    get canStopped(): boolean
    {
        return this.targetView && this._state === ReelState.Spinning;
    }
}

export enum ReelState
{
    Spinning,
    Stopping,
    Stopped,
    Winning
}