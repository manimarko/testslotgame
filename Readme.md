# Slot Machine

This project was created with pixi.js

### How to run

You can check out the online version of the project at:

https://slotgamenikitakulikov.herokuapp.com/

or

`npm run start` - made npm i and Runs the app in the development mode.
Open http://localhost:9000 to view it in the browser.
The page will reload if you make edits.

`npm run build` - Builds the app for production

### Project structure
This slot can be separated in two big parts: engine and game

The engine is made for provide component-based approach(like unity).
Here as in unity we work with GameObjects and GameObjects have no functionality, any functionality must be realized through the components.
For now in engine implemented basic components for work with pixi and layouting but work still in progress...

Game part splitted into modules

### EngineModule
just initialize the engine and loading all prefabs info

### GameModule
This module has some game specific components, actions and models
also here we build game flow with **ActionNodesBuilder**

main game flow is looped and consist from 4 action witch execute consequently

    nodesBuilder.get("Root.GameFlow").setArgs({looped: true});
    nodesBuilder.get("Root.GameFlow.Idle").setAction(IdleGameAction);
    nodesBuilder.get("Root.GameFlow.MakeSpin").setAction(MakeSpinAction);
    nodesBuilder.get("Root.GameFlow.CanStopSpinning").setAction(CanStopSpinningAction);
    nodesBuilder.get("Root.GameFlow.Winning").setAction(WinningsAction);

### DebugModule
just some stuff for debug




