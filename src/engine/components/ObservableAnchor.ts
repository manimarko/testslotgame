import { ObservableValue } from './ObservableValue';
import { AnchorType } from './CAnchor';

export class ObservableAnchor
{
	static RightAnchor = (pos, size, parentSize) => {
		return parentSize.calculatedValue - size.calculatedValue + pos.value;
	}
	static CenterAnchor = (pos, size, parentSize) => {
		return (parentSize.calculatedValue - size.calculatedValue) * 0.5 + pos.value;
	}
	static JustifyAnchor = (size, pos, parentSize) => {
		return parentSize.calculatedValue - pos.value - size.value;
	}

	pos: ObservableValue = new ObservableValue();
	size: ObservableValue = new ObservableValue();
	parentSize: ObservableValue = new ObservableValue();
	lPos: number = null;
	rPos: number = null;
	cPos: number = null;
	justify: Array<number> = null;
	type: AnchorType;

	constructor() {
		this.lPos = 0;
		this.update();
	}

	useLPos(pos: number) {
		if (this.type !== AnchorType.Left) {
			this.type = AnchorType.Left;
			this.pos.clear();
		}
		this.pos.setValue(pos);
	}

	useRPos(pos: number) {
		if (this.type !== AnchorType.Right) {
			this.type = AnchorType.Right;
			this.pos.clear();
			this.pos.addDependency(this.size);
			this.pos.addDependency(this.parentSize);
			this.pos.calculateFunc = ObservableAnchor.RightAnchor;
		}
		this.pos.setValue(pos);
	}

	useCPos(pos: number) {
		if (this.type !== AnchorType.Center) {
			this.type = AnchorType.Center;
			this.pos.clear();
			this.pos.addDependency(this.size);
			this.pos.addDependency(this.parentSize);
			this.pos.calculateFunc = ObservableAnchor.CenterAnchor;
		}
		this.pos.setValue(pos);
	}

	useJustify(hPos: Array<number>) {
		if (this.type !== AnchorType.Justify) {
			this.type = AnchorType.Justify;
			this.pos.clear();
			this.size.clear();
			this.size.addDependency(this.pos);
			this.size.addDependency(this.parentSize);
			this.size.calculateFunc = ObservableAnchor.JustifyAnchor;
		}
		this.pos.setValue(hPos[0]);
		this.size.setValue(hPos[1]);
	}

	update() {
		if (this.lPos != null) {
			this.useLPos(this.lPos);
		}
		if (this.rPos != null) {
			this.useRPos(this.rPos);
		}
		if (this.cPos != null) {
			this.useCPos(this.cPos);
		}
		if (this.justify != null) {
			this.useJustify(this.justify);
		}
	}

	clear() {
		this.lPos = this.rPos = this.cPos = this.justify = null;
	}
}
