import {AConstructorTypeOf, DI} from "../DI";

export enum NodeType
{
    SEQUENCE,
    PARALLEL
}

export class Node
{
    children: Node[] = [];
    type: NodeType = NodeType.SEQUENCE;
    childrenRunType: string = "first";
    name: string = "";
    looped: boolean = false;

    anonymousFunction: Function;
    args: any[] = [];

    action: IAction = null;

    setArgs(args: { looped?: boolean, type?: NodeType }): Node
    {
        Object.assign(this, args);
        return this;
    }

    setFunction(args, anonymousFunction: Function): Node
    {
        this.args = args;
        this.anonymousFunction = anonymousFunction;
        return this;
    }

    setAction(action: AConstructorTypeOf<IAction>): Node
    {
        this.action = new action();
        return this;
    }

    getChildByName(name: string): Node
    {
        return this.children.find(command => command.name === name);
    }

    protected async executeChildren(): Promise<any>
    {
        if (this.type === NodeType.PARALLEL) {
            const promises = this.children.reduce((promises, command) =>
            {
                promises.push(command.execute());
                return promises;
            }, []);
            await Promise.all(promises);
        } else {
            for (let i = 0; i < this.children.length; i++) {
                const command = this.children[i];
                const result = await command.execute();
                if (command.looped) {
                    i--;
                }
                if (!result) {
                    return;
                }
            }
        }
    }

    async execute(): Promise<any>
    {
        console.log("execute: " + this.name);
        if (this.children.length > 0 && this.childrenRunType === "first") {
            await this.executeChildren();
        }

        if (this.anonymousFunction) {
            const injectedArgs = this.args.reduce((injected, arg) =>
            {
                injected.push(DI.Inject(arg));
                return injected;
            }, []);
            await this.anonymousFunction.apply(this, injectedArgs);
        } else {
            if (this.action) {
                await this.action.execute();
            }
        }
        if (this.children.length > 0 && this.childrenRunType === "second") {
            await this.executeChildren();
        }
        return true;
    }

}

export interface IAction
{
    execute: () => Promise<void>
}