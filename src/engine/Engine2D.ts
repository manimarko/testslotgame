import {GameObjectBuilder} from './gameobject/GameObjectBuilder';
import {GameObject} from "./gameobject/GameObject";
import {CAnchor} from './components/CAnchor';
import {CContainer} from "./components/CContainer";
import {ResourceStorage} from './ResourceStorage';
import {CGraphics} from "./components/CGraphics";
import {CSprite} from "./components/CSprite";
import {CStateMachine} from "./components/CStateMachine";
import {CLayout} from "./components/CLayout";
import {CSlice9} from "./components/CSlice9";
import {CAutoFit} from "./components/CAutoFit";
import {CText} from "./components/CText";
import {CPivot} from "./components/CPivot";

export class Engine2D {
    static it: Engine2D = new Engine2D();
    private screenSizeRangeX: Array<number>;
    private screenSizeRangeY: Array<number>;
    private parentHTML: HTMLElement;
    public pixiApp: PIXI.Application;
    public root: GameObject;
    public prefabsData: any;
    public imagesData: any;

    init(data: IEngineInitSettings) {
        this.screenSizeRangeX = data.screenSizeRangeX;
        this.screenSizeRangeY = data.screenSizeRangeY;
        this.parentHTML = data.parentHTML;
        this.prefabsData = data.prefabsData;
        this.imagesData = data.imagesData;
        ResourceStorage.it.resourceGroups = data.resourceData;
        this.pixiApp = new PIXI.Application(data.screenSizeRangeX[0], data.screenSizeRangeY[0], {backgroundColor: data.bgColor});
        this.root = new GameObject();
        this.root.addComponent(CContainer);
        this.root.addComponent(CAnchor);
        this.root.name = "root";
        this.pixiApp.stage.addChild(this.root.compDict[CContainer.ID]);
        $(this.pixiApp.renderer.view).css("height", "100%");
        $(this.pixiApp.renderer.view).css("width", "100%");

        GameObjectBuilder.it.addComponents([
            CContainer,
            CGraphics,
            CSprite,
            CStateMachine,
            CLayout,
            CSlice9,
            CAnchor,
            CAutoFit,
            CText,
            CPivot]);

        window.addEventListener("resize", () => {
            this.resize();
        });

        this.root.start();
        this.root.compDict[CAnchor.ID].onAdded();
        this.parentHTML.appendChild(this.pixiApp.view);
        this.resize();
    }

    protected resize() {
        let minScreenW = this.screenSizeRangeX[0];
        let maxScreenW = this.screenSizeRangeX[1];
        let minScreenH = this.screenSizeRangeY[0];
        let maxScreenH = this.screenSizeRangeY[1];

        if (this.pixiApp.renderer) {
            let iW = $(window).width();
            let iH = $(window).height();

            let iW_ = iW;
            let iH_ = iH;
            let sX = 0;
            let sY = 0;

            if (iW_ < minScreenW || iH_ < minScreenH) {
                if (iW_ / minScreenW < iH_ / minScreenH) {
                    iW = minScreenW;
                    iH = iH_ * (minScreenW / iW_);
                } else {
                    iH = minScreenH;
                    iW = iW_ * (minScreenH / iH_);
                }
            }
            if (iW_ > maxScreenW || iH_ > maxScreenH) {
                if (iW_ / maxScreenW > iH_ / maxScreenH) {
                    iW = maxScreenW;
                    iH = iH_ * (maxScreenW / iW_);
                } else {
                    iH = maxScreenH;
                    iW = iW_ * (maxScreenH / iH_);
                }
            }

            let screenW = Math.max(minScreenW, Math.min(maxScreenW, iW));
            let screenH = Math.max(minScreenH, Math.min(maxScreenH, iH));

            if (screenW > iW && screenH < iH) {
                if (screenW > minScreenW) {
                    sX = (screenW / iW);
                    sY = 1;
                } else {
                }
            } else {
                if (screenH > iH && screenW < iW) {
                    if (screenH > minScreenH) {
                        sY = (screenH / iH);
                        sX = 1;
                    } else {
                    }
                } else {
                    let aspect = iW / iH;
                    let normal = screenW / screenH;
                    if (aspect > normal) {
                        sX = (normal / aspect);
                        sY = 1;
                    } else {
                        sY = (aspect / normal);
                        sX = 1;
                    }
                }
            }

            $(this.parentHTML).css("height", iH_ * sY + "px");
            $(this.parentHTML).css("width", iW_ * sX + "px");

            this.pixiApp.renderer.resize(screenW, screenH);

            $(this.parentHTML).css("margin-left", (iW_ - iW_ * sX) * 0.5 + "px");
            $(this.parentHTML).css("margin-top", (iH_ - iH_ * sY) * 0.5 + "px");

            const anchorComponent: CAnchor = this.root.compDict[CAnchor.ID];
            anchorComponent.xAnchors.size.setValue(screenW);
            anchorComponent.yAnchors.size.setValue(screenH);
        }
    }

}

export interface IEngineInitSettings {
    parentHTML: HTMLElement;
    screenSizeRangeX: Array<number>;
    screenSizeRangeY: Array<number>;
    bgColor: number;
    prefabsData: any;
    imagesData: any;
    resourceData: any;
}
