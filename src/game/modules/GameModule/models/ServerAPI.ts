export class ServerAPI
{
    get(data: { id: "init" | "spin", bet?: number }): Promise<any>
    {
        return Promise.resolve();
    }
}

export interface IInitData
{
    reelSet: number[][],
    balance: number,
    initialView: number[][],
}

export interface IWinline
{
    symbolPositions: number[],
    id: number,
    reward: number
}

export interface ISpinData
{
    view: number[][],
    winLines: IWinline[]
    balance: number,
    winAmount: number
}