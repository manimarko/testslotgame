import {DI} from "../../../../engine/DI";
import {ISpinData, ServerAPI} from "../models/ServerAPI";
import {ResponseParser} from "../models/ResponseParser";
import {IAction} from "../../../../engine/command/Node";
import {GameModel} from "../models/GameModel";
import EventEmitter = PIXI.utils.EventEmitter;

export class MakeSpinAction implements IAction
{
    emitter: EventEmitter = DI.Inject(EventEmitter);
    server: ServerAPI = DI.Inject(ServerAPI);
    responseParser: ResponseParser = DI.Inject(ResponseParser);
    gameModel: GameModel = DI.Inject(GameModel);

    async execute()
    {
        const serverResponse: ISpinData = await this.server.get({id: "spin", bet: this.gameModel.bet});
        this.responseParser.parse(serverResponse);
    }
}