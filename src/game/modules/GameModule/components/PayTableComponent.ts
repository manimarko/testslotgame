import {IComponent} from "../../../../engine/components/IComponent";
import {GameObject} from "../../../../engine/gameobject/GameObject";
import {IWinline} from "../models/ServerAPI";
import {ReelComponent} from "./ReelComponent";
import {DI} from "../../../../engine/DI";
import EventEmitter = PIXI.utils.EventEmitter;

export class PayTableComponent implements IComponent
{
    static ID: string = "PaytableComponent";
    gameObject: GameObject;
    emitter: EventEmitter = DI.Inject(EventEmitter);
    intervalId: number;
    ENABLED_ALPHA = 1;
    DISABLED_ALPHA = 0.3;

    start()
    {
        this.emitter.on(ReelComponent.SHOW_WIN_LINE, this.showWinLine, this);
        this.reset();
    }

    showWinLine(winLine: IWinline): void
    {
        this.reset();
        if (winLine) {
            const line = this.gameObject.getChildByName("line_" + winLine.id);
            const view = line.findComponentByKey("alpha");
            view.alpha = 1;
            this.intervalId = setInterval(() =>
            {
                view.alpha = view.alpha > this.DISABLED_ALPHA ? this.DISABLED_ALPHA : this.ENABLED_ALPHA;
            }, 300)
        }
    }

    reset()
    {
        clearInterval(this.intervalId);
        this.gameObject.children.forEach(child => child.findComponentByKey("alpha").alpha = this.DISABLED_ALPHA);
    }

    stop()
    {
    }

}