import {GameObject} from "../gameobject/GameObject";
import {IComponent} from "./IComponent";

export class CMaskRect extends PIXI.Graphics implements IComponent {
    static ID: string = "mask_rect";
    gameObject: GameObject;

    onChildAdded(child: GameObject) {

    }

    onChildRemoved(child: GameObject) {

    }

    start() {

    }

    set maskRect(v: string) {


    }

    stop() {

    }
}
