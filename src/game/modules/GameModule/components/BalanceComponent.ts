import {IComponent} from "../../../../engine/components/IComponent";
import {GameObject} from "../../../../engine/gameobject/GameObject";
import {DI} from "../../../../engine/DI";
import {CText} from "../../../../engine/components/CText";
import EventEmitter = PIXI.utils.EventEmitter;

export class BalanceComponent implements IComponent {
    static UPDATE_BALANCE = "UPDATE_BALANCE";
    static ID: string = "BalanceComponent";
    gameObject: GameObject;
    emitter: EventEmitter = DI.Inject(EventEmitter);
    textComponent: CText;

    start() {
        this.textComponent = this.gameObject.compDict[CText.ID];
        this.emitter.on(BalanceComponent.UPDATE_BALANCE, this.updateText, this);
    }

    updateText(text): void {
        this.textComponent.textValue = text;
    }

    stop() {
        this.emitter.removeListener(BalanceComponent.UPDATE_BALANCE, this.updateText);
    }
}