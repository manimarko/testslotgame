import {Engine2D} from './Engine2D';
import Texture = PIXI.Texture;

export class ResourceStorage {
    static it: ResourceStorage = new ResourceStorage();
    resourceGroups: any = {};

    async loadResourcesGroup(groupName: string): Promise<any> {
        let prefabNames = this.resourceGroups[groupName];
        let promises = [];
        for (let i = 0; i < prefabNames.length; i++) {
            await this.loadImagesForPrefab(prefabNames[i]);
        }
    }

    loadImagesForPrefab(prefabName: string): Promise<any> {
        let loadTextureData = [];
        Object.keys(Engine2D.it.imagesData[prefabName]).forEach(imageName => {
            if (!this.getTextureData(imageName)) {
                loadTextureData.push({name: imageName, url: './resources/images/' + imageName + ".png"});
            }
        });
        return this.loadResources(loadTextureData);
    }

    loadResources(resourcesData: Array<any>): Promise<any> {
        return new Promise((resolve, reject) => {
            PIXI.loader.add(resourcesData).load(() => {
                resolve();
            });
        });
    }

    getTextureData(id: string): Texture {
        return PIXI.loader.resources[id] ? PIXI.loader.resources[id].texture : null;
    }
}

