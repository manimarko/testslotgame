import {Engine2D} from "./Engine2D";
import {GameObject} from "./gameobject/GameObject";
import {GameObjectBuilder} from "./gameobject/GameObjectBuilder";

export class Pool {
    freeItems = [];
    usingItems = [];
    prefabName: string;

    constructor(name: string, initialCount: number) {
        this.prefabName = name;
        for (let i = 0; i < initialCount; i++) {
            this.freeItems.push(this.create());
        }
    }

    create(): GameObject {
        return GameObjectBuilder.it.createByName(this.prefabName);
    }

    get(): GameObject {
        if (this.freeItems.length === 0) {
            this.freeItems.push(this.create());
        }
        const obj = this.freeItems.pop();
        this.usingItems.push(obj);
        return obj;
    }

    back(item: GameObject) {
        this.freeItems.push(item);
        const index = this.usingItems.indexOf(item);
        if (index > -1) {
            this.usingItems.splice(index, 1);
        }
    }

    clear() {
        this.freeItems = [...this.freeItems, ...this.usingItems];
        this.usingItems = [];
    }
}

export class MultiItemsPool {
    pools = {};

    init(data) {
        Object.keys(data).forEach(key => {
            this.pools[key] = new Pool(key, data[key]);
        })
    }

    get(key): GameObject {
        return this.pools[key].get();
    }

    back(key, item: GameObject) {
        this.pools[key].back(item);
    }
}