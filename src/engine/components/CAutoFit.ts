import { GameObject } from "../gameobject/GameObject";
import { IComponent } from "./IComponent";
import { CAnchor } from './CAnchor';
import { ObservableAnchor } from "./ObservableAnchor";
import { ObservableValue } from "./ObservableValue";

export class DBounds {
    pos = new ObservableValue();
    size = new ObservableValue();
    children: Array<ObservableAnchor> = [];
    bindUpdate = this.update.bind(this);
    enabled: boolean = false;
    anchor: ObservableAnchor;

    add(obj: ObservableAnchor) {
        if (this.children.indexOf(obj) < 0) {
            this.children.push(obj);
            obj.pos.subscribe(this.bindUpdate);
            obj.size.subscribe(this.bindUpdate);
        }
    }

    remove(obj: ObservableAnchor) {
        let index = this.children.indexOf(obj);
        if (index > -1) {
            obj.pos.unsubscribe(this.bindUpdate);
            obj.size.unsubscribe(this.bindUpdate);
            this.children.splice(index, 1);
        }
    }

    update() {
        if (this.enabled) {
            let max = 0;
            let min = 0;
            for (let index = 0; index < this.children.length; index++) {
                const element = this.children[index];
                if (element.size.dependsValues.length == 0) {
                    min = Math.min(element.pos.calculatedValue, min);
                    max = Math.max(element.size.calculatedValue + element.pos.calculatedValue, max);
                }
            }
            this.pos.setValue(min);
            this.size.setValue(max - min);
            this.anchor.size.setValue(max - min);
        }
    }
}

export class CAutoFit implements IComponent {
    static ID: string = "autofit";
    gameObject: GameObject;
    selfAnchor: CAnchor;
    xBounds: DBounds = new DBounds();
    yBounds: DBounds = new DBounds();
    _xAutofit: boolean;
    _yAutofit: boolean;

    set xAutofit(v: boolean) {
        this._xAutofit = v;
        this.xBounds.enabled = v;
    }

    get xAutofit(): boolean {
        return this._xAutofit;
    }

    set yAutofit(v: boolean) {
        this._yAutofit = v;
        this.yBounds.enabled = v;
    }

    get yAutofit(): boolean {
        return this._yAutofit;
    }

    onChildAdded(child: GameObject) {
        const anchor: CAnchor = child.compDict[CAnchor.ID];
        this.xBounds.add(anchor.xAnchors);
        this.yBounds.add(anchor.yAnchors);
        this.xBounds.update();
        this.yBounds.update();
    }

    onChildRemoved(child: GameObject) {
        const anchor: CAnchor = child.compDict[CAnchor.ID];
        this.xBounds.remove(anchor.xAnchors);
        this.yBounds.remove(anchor.yAnchors);
        this.xBounds.update();
        this.yBounds.update();
    }

    start() {
        this.selfAnchor = <any>this.gameObject.compDict[CAnchor.ID];
        this.xBounds.anchor = this.selfAnchor.xAnchors;
        this.yBounds.anchor = this.selfAnchor.yAnchors;
        this.gameObject.children.forEach(element => {
            this.onChildAdded(element);
        });
        this.xBounds.update();
        this.yBounds.update();
    }

    stop() {

    }

}

