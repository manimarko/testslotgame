import { GameObject } from "../gameobject/GameObject";
import { IComponent } from "./IComponent";
import { ResourceStorage } from "../ResourceStorage";

export class CSprite extends PIXI.Sprite implements IComponent {
    static ID:string = "sprite";
    gameObject: GameObject;
    _pictId: string;
    
    onChildAdded(child: GameObject) {
        
    }
    onChildRemoved(child: GameObject) {
        
    }
    
    start() {
        this.name = this.gameObject.name;
    }

    set pictId(v: string) {
        this._pictId = v;
        this.texture = ResourceStorage.it.getTextureData(v);
    }
    get pictId(): string {
        return this._pictId;
    }
    stop() {
    }
}
