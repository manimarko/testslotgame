export const SYMBOLS = {
    symbol_CHERRY: 1,
    symbol_7: 2,
    symbol_3xBar: 3,
    symbol_2xBar: 4,
    symbol_1xBar: 5,
}