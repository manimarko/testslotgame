import {IModule} from "./Module";

export class GameStarter
{
    modules: IModule[];

    setModules(arr)
    {
        this.modules = arr.map(ModuleClass => new ModuleClass());
    }

    async execute()
    {
        await Promise.all(this.modules.map(module => module.init()));
        this.modules.forEach(module =>
        {
            module.run();
        });
    }
}