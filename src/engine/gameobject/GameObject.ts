import {IComponent} from "../components/IComponent";

export class GameObject
{
    children: GameObject[] = [];
    components: any[] = [];
    compDict: any = {};
    name: string = "";
    parent: GameObject;
    isStopped = false;
    isStarted = false;

    constructor(name = "")
    {
        this.name = name;
    }

    createAndAdd(name: string)
    {
        this.addChild(new GameObject(name));
    }

    private executeEventInComponent(component: any, eventName: string, args: any[] = null)
    {
        if (typeof component[eventName] === "function") {
            component[eventName].apply(component, args);
        }
    }

    executeEvent(eventName: string, args: any[] = null, recursive: boolean = false, componentClass: any = null, childrenFirst: boolean = false)
    {
        if (recursive && childrenFirst) {
            this.children.forEach(child =>
            {
                child.executeEvent(eventName, args, recursive, componentClass);
            })
        }
        if (componentClass) {
            if (this.compDict[componentClass.ID]) {
                this.executeEventInComponent(this.compDict[componentClass.ID], eventName, args);
            }
        } else {
            this.components.forEach(comp =>
            {
                this.executeEventInComponent(comp, eventName, args);
            });
        }

        if (recursive && !childrenFirst) {
            this.children.forEach(child =>
            {
                child.executeEvent(eventName, args, recursive, componentClass);
            })
        }
    }

    addChild(go: GameObject)
    {
        go.parent = this;
        go.executeEvent("onAdded");
        this.children.push(go);
        this.executeEvent('onChildAdded', [go]);
    }

    removeChild(go: GameObject)
    {
        go.executeEvent("onRemoved");
        this.children.splice(this.children.indexOf(go), 1);
        this.executeEvent('onChildRemoved', [go]);
    }

    removeChildren()
    {
        for (let i = 0; i < this.children.length; i++) {
            let go = this.children[i];
            go.executeEvent("onRemoved");
            this.executeEvent('onChildRemoved', [go]);
        }
        this.children.length = 0;
    }

    addComponent(componentClass: any)
    {
        if (this.compDict[componentClass.ID]) return null;
        const comp: IComponent = new componentClass();
        comp.gameObject = this;
        this.components.push(comp);
        this.compDict[componentClass.ID] = comp;
        return comp;
    }

    getChildByName(name: string): GameObject
    {
        return this.children.find(child => child.name == name);
    }

    start()
    {
        if (this.isStarted) {
            return;
        }
        this.isStarted = true;
        this.children.forEach(child => child.start());
        this.executeEvent('start', null, false);
    }

    stop()
    {
        if (this.isStopped) {
            return;
        }
        this.isStopped = true;
        this.children.forEach(child => child.stop());
        this.executeEvent('stop', null, true);
    }

    findComponentByKey(key: string)
    {
        return this.components.find((comp: any) =>
        {
            if (Reflect.has(comp, key)) {
                return comp;
            }
        });
    }

    find(path: string[], beginIndex: number = 0): any
    {
        let obj: GameObject = this;
        let index = beginIndex;
        while (true) {
            if (index >= path.length)
                return obj;
            let childName = path[index];
            if (obj.getChildByName(childName) == null) {
                let treeObj = new GameObject();
                treeObj.name = childName;
                obj.children.push(treeObj);
            }
            index++;
            obj = obj.getChildByName(childName);
        }
    }

}
