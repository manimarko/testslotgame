import {Engine2D} from '../Engine2D';
import {CStateMachine} from "../components/CStateMachine";
import {GameObject} from "./GameObject";

export interface IParsedData {
    path: string[];
    state: string;
    compValues: {};
    compNames: string[];
    pointers: [];
}

export class GameObjectBuilder {
    static it: GameObjectBuilder = new GameObjectBuilder();
    componentsMap: any = {};
    componentsMapByValues: any = {};
    rules = [];

    addRule(guard: (go: GameObject) => boolean, action: (go: GameObject) => any): any {
        this.rules.push([guard, action]);
    }

    createByName(name: string): GameObject {
        const prefab = this.createFromData(Engine2D.it.prefabsData[name]);
        prefab.name = name;
        return prefab;
    }

    createFromData(data: any): GameObject {
        const obj = this.executeCommands(data);
        obj.start();
        return obj;
    }

    addComponents(componentClasses: any[]) {
        for (let i = 0; i < componentClasses.length; i++) {
            this.addComponent(componentClasses[i]);
        }
    }

    addComponent(componentClass: any) {
        this.componentsMap[componentClass.ID] = componentClass;
        let arr = this.getInstanceMethodNames(componentClass.prototype);
        arr.forEach(element => {
            if (!this.componentsMapByValues[element]) {
                this.componentsMapByValues[element] = [];
            }
            this.componentsMapByValues[element].push(componentClass);
        });
    }

    executeCommands(data: IParsedData[]) {
        data = JSON.parse(JSON.stringify(data));
        let root = new GameObject();
        root.addComponent(CStateMachine);

        for (let i = 0; i < data.length; i++) {
            this.executeComponentCommands(data[i], root);
        }

        for (let i = 0; i < data.length; i++) {
            this.executePointersCommands(data[i], root);
        }

        for (let i = 0; i < data.length; i++) {
            this.executeValueCommands(data[i], root);
        }

        this.prepare(root);

        this.add(root);

        return root;
    }

    private hasMethod(obj: any, name: string) {
        const desc = Object.getOwnPropertyDescriptor(obj, name);
        return !!desc && !!desc.set;
    }

    private getInstanceMethodNames(proto, stop = null) {
        let array = [];
        while (proto && proto !== stop) {
            Object.getOwnPropertyNames(proto)
                .forEach(name => {
                    if (name !== 'constructor' && name != '__proto__') {
                        if (this.hasMethod(proto, name)) {
                            array.push(name);
                        }
                    }
                });
            proto = Object.getPrototypeOf(proto);
        }
        return array;
    }

    private add(obj: GameObject): void {
        if (obj.parent) {
            obj.executeEvent("onAdded");
        }
        obj.children.forEach(c => this.add(c));
    }

    private prepare(obj: GameObject): void {
        this.rules.forEach(rule => {
            if (rule[0](obj)) {
                rule[1](obj);
            }
        });

        obj.children.forEach(child => {
            this.prepare(child);
        })
    }

    private getDisplayObject(child: GameObject) {
        return child.findComponentByKey('visible');
    }

    private executeComponentCommands(data: IParsedData, root: GameObject) {
        const currGameObject = this.createByPathArray(root, data.path);
        this.addComponentsToGameObject(data, currGameObject);
    }

    private executePointersCommands(data: IParsedData, root: GameObject) {
        const currGameObject = this.createByPathArray(root, data.path);
        this.addObjects(data, currGameObject);
    }

    private executeValueCommands(data: IParsedData, root: GameObject) {
        const currGameObject = this.createByPathArray(root, data.path);
        this.addValues(data, currGameObject, root);
    }

    private addComponentsToGameObject(data: IParsedData, obj: GameObject) {
        for (let i = 0; i < data.compNames.length; i++) {
            let compClass = this.componentsMap[data.compNames[i]];
            if (compClass) {
                obj.addComponent(compClass);
            }
        }
    }

    private addObjects(data: IParsedData, obj: GameObject) {
        if (data.pointers) {
            for (let i = 0; i < data.pointers.length; i++) {
                const name = data.pointers[i];
                obj.addChild(this.createByName(name));
            }
        }
    }

    private addComponentByKey(obj: GameObject, key: string) {
        let array = this.componentsMapByValues[key];
        if (array && array.length > 0) {
            return obj.addComponent(array[0]);
        }
        return null;
    }

    private addValues(data: IParsedData, obj: GameObject, root: GameObject) {
        for (const key in data.compValues) {
            if (data.compValues.hasOwnProperty(key)) {
                let comp = obj.findComponentByKey(key);

                if (!comp) {
                    comp = this.addComponentByKey(obj, key);
                }

                if (comp) {
                    if (data.state) {
                        root.compDict[CStateMachine.ID].addStateData(data.state, data);
                    } else {
                        comp[key] = data.compValues[key];
                    }
                } else {
                    console.log(`component with ${key} not exist`);
                }
            }
        }
    }

    private createByPathArray(obj: GameObject, path: Array<string>): GameObject {
        let index = 1;
        while (true) {
            if (index >= path.length)
                return obj;
            let childName = path[index];
            if (obj.getChildByName(childName) == null) {
                let treeObj = new GameObject();
                treeObj.name = childName;
                obj.children.push(treeObj);
                treeObj.parent = obj;
            }
            index++;
            obj = obj.getChildByName(childName) as GameObject;
        }
    }
}

