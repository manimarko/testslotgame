import { GameObject } from "../gameobject/GameObject";
import { IComponent } from "./IComponent";
import { ResourceStorage } from "../ResourceStorage";

export class CSlice9 extends PIXI.mesh.NineSlicePlane implements IComponent {
    static ID: string = "slice9";
    gameObject: GameObject;
    _pictId: string;

    constructor() {
        super(PIXI.Texture.WHITE, 10, 10, 10, 10);
    }

    set sPictId(v: string) {
        this._pictId = v;
        this.texture = ResourceStorage.it.getTextureData(v);
    }
    get sPictId(): string {
        return this._pictId;
    }

    set sliceInfo(v: number[]) {
        this.leftWidth = v[0];
        this.topHeight = v[1];
        this.rightWidth = v[2];
        this.bottomHeight = v[3];
    }

    get sliceInfo(): number[] {
        return [
            this.leftWidth,
            this.topHeight,
            this.rightWidth,
            this.bottomHeight];
    }
}
