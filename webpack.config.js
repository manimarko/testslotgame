const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const distPath = path.join(__dirname, '/dist');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: "./src/index.ts",

    devtool: "inline-source-map",

    optimization: {
        removeAvailableModules: false,
        removeEmptyChunks: false,
        splitChunks: false,
        minimize: false
    },

    resolve: {
        extensions: [".ts", ".js"]
    },

    output: {
        pathinfo: false
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [{
                    loader: 'ts-loader',
                    options: {
                        transpileOnly: true,
                        experimentalWatchApi: true,
                    }
                }],
                exclude: /node_modules/
            }
        ]
    },

    resolve: {
        extensions: [ '.ts', '.tsx', '.js' ]
    },

    plugins: [
        new ForkTsCheckerWebpackPlugin(),
        new CopyWebpackPlugin([
            { from: 'node_modules/pixi.js/dist/pixi.min.js', to: '' },
            { from: 'node_modules/jquery/dist/jquery.js', to: '' },
        ], {}),
        new HtmlWebpackPlugin({
            title: 'Custom template',
            // Load a custom template (lodash by default see the FAQ for details)
            template: './src/template.html'
        })
    ],

    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'main.js'        
    },

    devServer: {
        port: 9000,
        compress: true,
        open: true,
        contentBase: distPath,
        publicPath: '/'
    }
};