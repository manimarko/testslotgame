import { GameObject } from "../gameobject/GameObject";
import { IComponent } from "./IComponent";
export class CGraphics extends PIXI.Graphics implements IComponent {
    static ID:string = "graphics";
    gameObject: GameObject;
    _size:number[] = [0,0];

    onChildAdded(child: GameObject) {

    }
    
    onChildRemoved(child: GameObject) {

    }

    start() {
        
    }

    draw() {
        this.clear();
        this.beginFill(0xFF6633, 1);
        this.drawRect(0, 0, this.size[0], this.size[1]);
        this.endFill();
    }

    stop() {
    }
    
    set size(v:number[]) {
        this._size = v;
        this.draw();
    }

    get size():number[] {
        return this._size;
    }
}
