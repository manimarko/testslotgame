import {IModule} from "../../../engine/Module";
import {DI} from "../../../engine/DI";
import {Emulator, ServerAPI} from "../GameModule/models/ServerAPI";
import {ResponseParser} from "../GameModule/models/ResponseParser";
import {BalanceComponent} from "../GameModule/components/BalanceComponent";
import {ReelsModel} from "../GameModule/models/ReelsModel";
import * as dat from 'dat.gui';
import EventEmitter = PIXI.utils.EventEmitter;

export class DebugModule implements IModule
{
    async init()
    {
    }

    run(): void
    {
        const datGUIData = {
            reelSet1: JSON.stringify((DI.Inject(ServerAPI) as Emulator).reelsSet[0]),
            reelSet2: JSON.stringify((DI.Inject(ServerAPI) as Emulator).reelsSet[1]),
            reelSet3: JSON.stringify((DI.Inject(ServerAPI) as Emulator).reelsSet[2]),
            balance: (DI.Inject(ServerAPI) as Emulator).balance,
            fixedReelsPosition: '[]',
            setReels()
            {
                let reelsSet;
                try {
                    reelsSet = [JSON.parse(this.reelSet1), JSON.parse(this.reelSet2), JSON.parse(this.reelSet3)];
                    if (reelsSet.some(reel => reel.length === 0)) {
                        return;
                    }
                } catch (e) {
                    return;
                }
                (DI.Inject(ServerAPI) as Emulator).reelsSet = reelsSet;
                DI.Inject(ReelsModel).reels.forEach((reel, i) => reel.reelSet = reelsSet[i]);
            },
            setFixedReelsPosition()
            {
                let fixedReelsPosition;
                try {
                    fixedReelsPosition = JSON.parse(this.fixedReelsPosition);
                } catch (e) {
                    return;
                }
                (DI.Inject(ServerAPI) as Emulator).fixedPositions = fixedReelsPosition;
            }
        };

        const gui = new dat.gui.GUI(datGUIData);

        gui.add(datGUIData, "balance", 0, 5000, 1).listen().onChange((value) =>
        {
            const emulator = (DI.Inject(ServerAPI) as Emulator);
            emulator.balance = Math.min(Math.max(0, value), 5000);
            const parser = DI.Inject(ResponseParser);
            parser.balance = emulator.balance;
            const emitter = DI.Inject(EventEmitter);
            emitter.emit(BalanceComponent.UPDATE_BALANCE, parser.balance);
        });

        gui.add(datGUIData, "reelSet1");
        gui.add(datGUIData, "reelSet2");
        gui.add(datGUIData, "reelSet3");
        gui.add(datGUIData, "setReels");

        gui.add(datGUIData, "fixedReelsPosition");
        gui.add(datGUIData, "setFixedReelsPosition");
    }
}