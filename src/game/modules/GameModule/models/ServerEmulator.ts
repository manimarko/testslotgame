import {SYMBOLS} from "./Symbols";
import {PayLinesModel} from "./PayLinesModel";
import {DI} from "../../../../engine/DI";
import {IInitData, ISpinData, ServerAPI} from "./ServerAPI";

export class ServerEmulator extends ServerAPI
{
    reelsSet =
        [
            [SYMBOLS.symbol_3xBar, SYMBOLS.symbol_1xBar, SYMBOLS.symbol_2xBar, SYMBOLS.symbol_7, SYMBOLS.symbol_CHERRY],
            [SYMBOLS.symbol_3xBar, SYMBOLS.symbol_1xBar, SYMBOLS.symbol_2xBar, SYMBOLS.symbol_7, SYMBOLS.symbol_CHERRY],
            [SYMBOLS.symbol_3xBar, SYMBOLS.symbol_1xBar, SYMBOLS.symbol_2xBar, SYMBOLS.symbol_7, SYMBOLS.symbol_CHERRY]
        ]
    reelViewSize = 3;
    balance = 100;
    payLinesModel: PayLinesModel = DI.Inject(PayLinesModel);
    fixedPositions: number[];

    async get(data: any): Promise<IInitData | ISpinData>
    {
        await new Promise(resolve => setTimeout(resolve, 500 + Math.random() * 500));
        if (data.id === "init") {
            return {
                reelSet: this.reelsSet.map(reel => [...reel]),
                balance: this.balance,
                initialView: this.reelsSet.map(reel => [...reel]),
            }
        }
        if (data.id === "spin") {
            this.balance -= data.bet;

            let positions = (!this.fixedPositions || this.fixedPositions.length === 0) ? this.reelsSet.map(reel => Math.floor(Math.random() * reel.length)) : this.fixedPositions;

            let reelView = [];
            let reelViewForPaytable = [];

            for (let i = 0; i < positions.length; i++) {
                let pos = positions[i];
                const reel = this.reelsSet[i];
                reelView[i] = [];
                for (let j = 0; j < this.reelViewSize; j++) {
                    reelView[i].push(reel[pos]);
                    if (!reelViewForPaytable[j]) {
                        reelViewForPaytable[j] = new Array(reel.length);
                    }
                    reelViewForPaytable[j][i] = {symbol: reel[pos], pos: [j, i]};
                    pos++;
                    if (pos >= reel.length) {
                        pos -= reel.length;
                    }
                }
            }
            const winLines = this.payLinesModel.calculate(reelViewForPaytable);
            const winAmount = winLines.reduce((sum, winLine) => sum + winLine.reward, 0);
            this.balance += winAmount;
            return {view: reelView, winLines: winLines, balance: this.balance, winAmount};
        }
    }
}