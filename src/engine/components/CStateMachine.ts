import {GameObject} from "../gameobject/GameObject";
import {IComponent} from "./IComponent";
import {IParsedData} from "../prefabs/GameObjectBuilder";

export class CStateMachine implements IComponent {
    static ID: string = "states";
    gameObject: GameObject;
    states: { [key: string]: IParsedData[] } = {};
    lastState: string = "default";

    onChildAdded(child: GameObject) {
    }

    addStateData(stateName: string, currComp: IParsedData) {
        if (!this.states[stateName]) {
            this.states[stateName] = [];
        }
        this.states[stateName].push(currComp);
    }

    setState(state: string) {
        this.lastState = state;
        let commands = this.states[state];
        if (commands) {
            commands.forEach(element => {
                this.updateComponents(element);
            });
        } else {
            console.log(`state ${state} not exist`);
        }
    }

    private updateComponents(data: IParsedData) {
        const current = this.gameObject.find(data.path, 1);
        for (const key in data.compValues) {
            let currComp = current.findComponentByKey(key);
            Reflect.set(currComp, key, data.compValues[key]);
        }
    }

    private createDefaultState(): void {
        let defaultStateCommands = [];
        for (const key in this.states) {
            if (key != "default") {
                if (this.states.hasOwnProperty(key)) {
                    const stateCommands = this.states[key];
                    stateCommands.forEach(element => {
                        defaultStateCommands.push(this.getAllDataComponents(element, key));
                    });
                }
            }
        }

        this.states["default"] = defaultStateCommands;
    }

    private getAllDataComponents(data: IParsedData, stateName: string): IParsedData {
        let currentData: IParsedData = {path: data.path, pointers: [], compValues: {}, compNames: [], state: stateName};
        currentData.path = data.path;
        const current = this.gameObject.find(data.path, 1);
        for (const key in data.compValues) {
            let currComp = current.findComponentByKey(key);
            currentData.compValues[key] = Reflect.get(currComp, key);
        }
        return currentData;
    }

    onChildRemoved(child: GameObject) {
    }

    start() {
        this.createDefaultState();
    }

    stop() {
    }
}
