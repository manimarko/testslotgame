const path = require('path'), fs = require('fs');
const {createCanvas} = require('canvas');

let currPrefabName = "";

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function saveObject(path, data) {
    fs.writeFileSync(path, JSON.stringify(data, null, '\t'));
}

function getAllTextFromFolder(path) {
    const allLines = [];
    let files = fs.readdirSync(path);
    files.forEach(file => {
        allLines.push(...fs.readFileSync(path + file, "utf-8").replace(/\r/g, '').split('\n'));
    });
    return allLines;
}

class CommandParser {

    constructor() {
        this.parsedCommands = [];
        this.prefabsMap = {};
    }

    parseComponentValues(str) {
        return str.split("|").reduce((map, obj) => {
            let arr = obj.split("=");
            let value = arr.length > 1 ? arr[1] : "";
            if (value.indexOf(',') > 0) {
                let values = value.split(',');
                map[arr[0]] = values.map(x => {
                    if (isNumber(x)) {
                        return parseFloat(x);
                    }
                    if (x === "true" || x === "false") {
                        return x === "true";
                    }
                    return x;
                });
            } else {
                if (isNumber(value)) {
                    map[arr[0]] = parseFloat(value);
                } else {
                    if (value === "true" || value === "false") {
                        map[arr[0]] = value === "true";
                    } else {
                        map[arr[0]] = value;
                    }
                }
            }
            return map;
        }, {});
    }

    parseComponentNames(str) {
        return str.split('|');
    }

    parseCommand(str) {
        if (str.indexOf('-') == 0) {
            return null;
        }

        if (str.indexOf('//') == 0) {
            this.currPrefabName = str.replace('//', '');
            return null;
        }

        let parsedData = {prefabName: this.currPrefabName};

        let state = null;
        const separatorComp = "::";
        const separatorValues = "->";
        const separatorPointer = "+";

        if (str.charAt(0) == "$") {
            let index = str.indexOf('>');
            state = str.substring(1, index);
            str = str.substring(index + 1, str.length);
        }

        let arr = [];
        parsedData.compNames = [];
        parsedData.compValues = {};
        if (str.indexOf(separatorComp) > -1) {
            arr = str.split(separatorComp);
            parsedData.compNames = this.parseComponentNames(arr[1]);
        } else if (str.indexOf(separatorValues) > -1) {
            arr = str.split(separatorValues);
            parsedData.compValues = this.parseComponentValues(arr[1]);
        } else if (str.indexOf(separatorPointer) > -1) {
            arr = str.split(separatorPointer);
            parsedData.pointers = this.parseComponentNames(arr[1]);
        } else {
            arr[0] = str;
        }

        parsedData.path = arr[0].split('.');
        parsedData.state = state ? state : parsedData.state;
        return parsedData;
    }

    parseCommands(lines) {
        for (let i = 0; i < lines.length; i++) {
            const parsedCommand = this.parseCommand(lines[i]);
            if (parsedCommand) {
                this.parsedCommands.push(parsedCommand);
            }
        }
        this.prefabsMap = this.parsedCommands.reduce(function (map, obj) {
            if (!map[obj.prefabName]) {
                map[obj.prefabName] = [];
            }
            map[obj.prefabName].push(obj);
            return map;
        }, {});
    }
}

function parseFolder(sourceFolder = './resources', destFolder = './dist/resources') {
    fs.mkdirSync(destFolder, {recursive: true});
    //generate prefabs
    const commandParser = new CommandParser();
    commandParser.parseCommands(getAllTextFromFolder(sourceFolder + '/prefabs/'));

    saveObject(destFolder + "/prefabs.json", commandParser.prefabsMap);

    Object.keys(commandParser.prefabsMap).forEach(key => console.log(key + " was generated."));
}

parseFolder();

console.log("complete");