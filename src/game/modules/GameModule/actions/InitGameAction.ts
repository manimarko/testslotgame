import {DI} from "../../../../engine/DI";
import {ServerAPI} from "../models/ServerAPI";
import {ResponseParser} from "../models/ResponseParser";
import {BalanceComponent} from "../components/BalanceComponent";
import {ReelsModel} from "../models/ReelsModel";
import {Engine2D} from "../../../../engine/Engine2D";
import {GameObjectBuilder} from "../../../../engine/gameobject/GameObjectBuilder";
import {MultiItemsPool} from "../../../../engine/Pool";
import {ResourceStorage} from "../../../../engine/ResourceStorage";
import {IAction, Node} from "../../../../engine/command/Node";
import EventEmitter = PIXI.utils.EventEmitter;

export class InitGameAction implements IAction
{
    emitter: EventEmitter = DI.Inject(EventEmitter);
    server: ServerAPI = DI.Inject(ServerAPI);
    reelModel: ReelsModel = DI.Inject(ReelsModel);
    multiItemsPool: MultiItemsPool = DI.Inject(MultiItemsPool);
    responseParser: ResponseParser = DI.Inject(ResponseParser);

    async execute()
    {
        const [initialData] = await Promise.all([this.server.get({id: "init"}), ResourceStorage.it.loadResourcesGroup("main")]);
        this.multiItemsPool.init({
            symbol_1: 5,
            symbol_2: 5,
            symbol_3: 5,
            symbol_4: 5,
            symbol_5: 5
        })
        this.reelModel.init(initialData.reelSet, initialData.initialView);
        this.responseParser.balance = initialData.balance;
        Engine2D.it.root.addChild(GameObjectBuilder.it.createByName("game"));
        this.emitter.emit(BalanceComponent.UPDATE_BALANCE, initialData.balance);
    }
}