import { CAnchor } from './CAnchor';
import { IComponent } from "./IComponent";
import { GameObject } from '../gameobject/GameObject';

export class CPivot implements IComponent {
    static ID: string = "pivot";
    gameObject: GameObject;
    pivot: Array<number> = [0, 0];
    visibleComponent: PIXI.Container | PIXI.Sprite | PIXI.mesh.NineSlicePlane;
    anchor: CAnchor;

    start() {
        this.visibleComponent = this.gameObject.findComponentByKey('visible');
        this.anchor = this.gameObject.compDict[CAnchor.ID];
        this.updatePivot();
    }

    updatePivot() {
        if (this.visibleComponent) {
            this.visibleComponent.pivot.x = this.pivot[0] * (this.anchor ? this.anchor.xAnchors.size : this.visibleComponent.width);
            this.visibleComponent.pivot.y = this.pivot[1] * (this.anchor ? this.anchor.yAnchors.size : this.visibleComponent.height);
        }
    }

    stop() {

    }

    set pPoint(v: Array<number>) {
        this.pivot = v;
        this.updatePivot();
    }

    get pPoint(): Array<number> {
        return this.pivot;
    }
}
