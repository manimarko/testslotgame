import {CPivot} from './CPivot';
import {GameObject} from "../gameobject/GameObject";
import {IComponent} from "./IComponent";
import {ObservableAnchor} from './ObservableAnchor';
import {CText} from "./CText";
import {CSprite} from "./CSprite";

export class CAnchor implements IComponent {
    static ID: string = "anchor";
    gameObject: GameObject;
    visibleComponent: PIXI.Container | PIXI.Sprite | PIXI.mesh.NineSlicePlane;
    pivot: CPivot;
    xAnchors = new ObservableAnchor();
    yAnchors = new ObservableAnchor();

    viewType = ViewType.Container;
    updateHBind = this.updateH.bind(this);
    updateVBind = this.updateV.bind(this);

    constructor() {
    }

    onChildAdded(child: GameObject) {

    }

    onAdded() {
        if (this.gameObject.parent) {
            let compDict: CAnchor = this.gameObject.parent.compDict[CAnchor.ID];
            if (compDict) {
                this.xAnchors.parentSize.addDependency(compDict.xAnchors.size);
                this.xAnchors.parentSize.calculateFunc = (v) => {
                    return compDict.xAnchors.size.calculatedValue;
                }
                this.yAnchors.parentSize.addDependency(compDict.yAnchors.size);
                this.yAnchors.parentSize.calculateFunc = (v) => {
                    return compDict.yAnchors.size.calculatedValue;
                }
                this.xAnchors.parentSize.update();
                this.yAnchors.parentSize.update();
            }
        }
    }

    onChildRemoved(child: GameObject) {
    }

    start() {
        this.visibleComponent = <any>this.gameObject.findComponentByKey("visible");
        this.pivot = this.gameObject.compDict[CPivot.ID];

        this.viewType = ViewType.Container;
        if (this.gameObject.compDict[CSprite.ID]) {
            this.viewType = ViewType.Sprite;
        } else if (this.gameObject.compDict[CText.ID]) {
            this.viewType = ViewType.Text;
        }

        this.xAnchors.pos.subscribe(this.updateHBind);
        this.yAnchors.pos.subscribe(this.updateVBind);
        this.xAnchors.size.subscribe(this.updateHBind);
        this.yAnchors.size.subscribe(this.updateVBind);

        this.xAnchors.update();
        this.yAnchors.update();

        if (this.viewType === ViewType.Sprite) {
            if (this.xAnchors.size.dependsValues.length == 0) {
                if (this.xAnchors.size.value === 0) {
                    this.w = this.visibleComponent.width;
                }
                this.visibleComponent.width = this.xAnchors.size.calculatedValue;
            }
            if (this.yAnchors.size.dependsValues.length == 0) {
                if (this.yAnchors.size.value === 0) {
                    this.h = this.visibleComponent.height;
                }
                this.visibleComponent.height = this.yAnchors.size.calculatedValue;
            }
        }
        if (this.viewType === ViewType.Text) {
            (<CText>this.gameObject.compDict[CText.ID]).addListener(<any>"text_changed", () => {
                this.updateSizeForText();
            });
            this.updateSizeForText();
        }
    }

    updateSizeForText() {
        this.w = this.visibleComponent.width;
        this.h = this.visibleComponent.height;
    }

    stop() {

    }

    updateH() {
        if (this.pivot) {
            this.pivot.updatePivot();
        }
        if (this.visibleComponent) {
            this.visibleComponent.x = this.xAnchors.pos.calculatedValue + this.visibleComponent.pivot.x;

            if (this.viewType === ViewType.Sprite) {
                this.visibleComponent.width = this.xAnchors.size.calculatedValue;
            }
        }
    }

    updateV() {
        if (this.pivot) {
            this.pivot.updatePivot();
        }
        if (this.visibleComponent) {
            this.visibleComponent.y = this.yAnchors.pos.calculatedValue + this.visibleComponent.pivot.y;

            if (this.viewType === ViewType.Sprite) {
                this.visibleComponent.height = this.yAnchors.size.calculatedValue;
            }
        }
    }

    set w(v: number) {
        this.xAnchors.size.setValue(v);
    }

    set h(v: number) {
        this.yAnchors.size.setValue(v);
    }

    set left(v: number) {
        this.xAnchors.clear();
        this.xAnchors.lPos = v;
        this.xAnchors.update();
    }

    set right(v: number) {
        this.xAnchors.clear();
        this.xAnchors.rPos = v;
        this.xAnchors.update();
    }

    set centerH(v: number) {
        this.xAnchors.clear();
        this.xAnchors.cPos = v;
        this.xAnchors.update();
    }

    set justifyH(v: Array<number>) {
        this.xAnchors.clear();
        this.xAnchors.justify = v;
        this.xAnchors.update();
    }

    set top(v: number) {
        this.yAnchors.clear();
        this.yAnchors.lPos = v;
        this.yAnchors.update();
    }

    set bottom(v: number) {
        this.yAnchors.clear();
        this.yAnchors.rPos = v;
        this.yAnchors.update();
    }

    set centerV(v: number) {
        this.yAnchors.clear();
        this.yAnchors.cPos = v;
        this.yAnchors.update();
    }

    set justifyV(v: Array<number>) {
        this.yAnchors.clear();
        this.yAnchors.justify = v;
        this.yAnchors.update();
    }

}

export enum ViewType {
    Container,
    Sprite,
    Text
}

export enum AnchorType {
    Left,
    Right,
    Center,
    Justify
}