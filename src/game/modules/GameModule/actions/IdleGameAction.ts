import {DI} from "../../../../engine/DI";
import {ResponseParser} from "../models/ResponseParser";
import {BalanceComponent} from "../components/BalanceComponent";
import {ReelsModel} from "../models/ReelsModel";
import {IAction} from "../../../../engine/command/Node";
import {PlayBtnComponent} from "../components/PlayButtonComponent";
import {WinAmountComponent} from "../components/WinAmountComponent";
import {GameModel} from "../models/GameModel";
import EventEmitter = PIXI.utils.EventEmitter;

export class IdleGameAction implements IAction
{
    emitter: EventEmitter = DI.Inject(EventEmitter);
    reelModel: ReelsModel = DI.Inject(ReelsModel);
    responseParser: ResponseParser = DI.Inject(ResponseParser);
    gameModel: GameModel = DI.Inject(GameModel);

    async execute()
    {
        if (!this.gameModel.isForceSpin) {
            await new Promise(resolve => this.emitter.once(PlayBtnComponent.START_SPIN, resolve));
        }
        this.gameModel.isForceSpin = false;
        this.reelModel.start();
        this.emitter.emit(PlayBtnComponent.SET_STATE, PlayBtnComponent.DISABLED);
        this.emitter.emit(WinAmountComponent.UPDATE_WIN_AMOUNT, "GOOD LUCK");
        this.emitter.emit(BalanceComponent.UPDATE_BALANCE, this.responseParser.balance - this.gameModel.bet);
    }
}