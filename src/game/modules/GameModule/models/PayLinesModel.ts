import {SYMBOLS} from "./Symbols";
import {IWinline} from "./ServerAPI";

export class PayLinesModel
{
    protected payLinesInfo = [
        //3 CHERRY symbols on top line 2000
        {
            id: 1, isPaylineExist: (view) =>
            {
                return this.isCountSymbol(view[0], SYMBOLS.symbol_CHERRY, 3)
            },
            reward: 2000,
            description: "3 CHERRY symbols on top line 2000"
        },
        //3 CHERRY symbols on center line 1000
        {
            id: 2, isPaylineExist: (view) =>
            {
                return this.isCountSymbol(view[1], SYMBOLS.symbol_CHERRY, 3)
            },
            reward: 1000,
            description: "3 CHERRY symbols on center line 1000"
        },
        //3 CHERRY symbols on bottom line 4000
        {
            id: 3, isPaylineExist: (view) =>
            {
                return this.isCountSymbol(view[2], SYMBOLS.symbol_CHERRY, 3)
            },
            reward: 4000,
            description: "3 CHERRY symbols on bottom line 4000"
        },
        //3 7 symbols on any line 150
        {
            id: 4, isPaylineExist: (view) =>
            {
                return view.some(line =>
                {
                    const result = this.isCountSymbol(line, SYMBOLS.symbol_7, 3);
                    if (!result) {
                        this.matchedSymbolPositions.length = 0;
                    }
                    return result;
                })
            },
            reward: 150
        },
        //Any combination of CHERRY and 7 on any line 75
        {
            id: 5,
            reward: 75, isPaylineExist: (view) =>
            {
                return view.some(line =>
                {
                    const result = this.isSomeSymbol(line, SYMBOLS.symbol_7) && this.isSomeSymbol(line, SYMBOLS.symbol_CHERRY);
                    if (!result) {
                        this.matchedSymbolPositions.length = 0;
                    }
                    return result;
                });
            }
        },
        //3 3xBAR symbols on any line 50
        {
            id: 6, reward: 50, isPaylineExist: (view) =>
            {
                return view.some(line =>
                {
                    const result = this.isCountSymbol(line, SYMBOLS.symbol_3xBar, 3);
                    if (!result) {
                        this.matchedSymbolPositions.length = 0;
                    }
                    return result;
                })
            }
        },
        //3 2xBAR symbols on any line 20
        {
            id: 7, reward: 20, isPaylineExist: (view) =>
            {
                return view.some(line =>
                {
                    const result = this.isCountSymbol(line, SYMBOLS.symbol_2xBar, 3)
                    if (!result) {
                        this.matchedSymbolPositions.length = 0;
                    }
                    return result;
                })
            }
        },
        //3 BAR symbols on any line 10
        {
            id: 8, reward: 10, isPaylineExist: (view) =>
            {
                return view.some(line =>
                {
                    const result = this.isCountSymbol(line, SYMBOLS.symbol_1xBar, 3)
                    if (!result) {
                        this.matchedSymbolPositions.length = 0;
                    }
                    return result;
                })
            }
        },
        //Combination of any BAR symbols on any line give 5,
        {
            id: 9, reward: 5, isPaylineExist: (view) =>
            {
                return view.some(line =>
                {
                    const result = this.isSomeSymbol(line, SYMBOLS.symbol_1xBar)
                    if (!result) {
                        this.matchedSymbolPositions.length = 0;
                    }
                    return result;
                });
            },
        }];

    protected matchedSymbolPositions = [];

    protected isSomeSymbol(arr, symbolId)
    {
        return arr.some(symbol =>
        {
            if (symbol.symbol === symbolId && !symbol.passed) {
                symbol.passed = true;
                this.matchedSymbolPositions.push(symbol.pos);
                return true;
            }
            return false;
        });
    }

    protected clear(arr)
    {
        return arr.forEach(reel => reel.forEach(symbol => symbol.passed = false));
    }

    protected isCountSymbol(arr, symbolId, count)
    {
        return arr.reduce((acc, symbol) =>
        {
            if (symbol.symbol === symbolId && !symbol.passed) {
                symbol.passed = true;
                this.matchedSymbolPositions.push(symbol.pos);
                return acc + 1;
            }
            return acc;
        }, 0) === count;
    }

    public calculate(reelView): IWinline[]
    {
        const winlines = [];
        this.payLinesInfo.forEach(item =>
        {
            this.clear(reelView);
            this.matchedSymbolPositions.length = 0;
            while (true) {
                const isPayline = item.isPaylineExist(reelView);
                if (!isPayline || this.matchedSymbolPositions.length === 0) {
                    this.matchedSymbolPositions.length = 0;
                    return;
                }
                if (isPayline) {
                    winlines.push({
                        symbolPositions: [...this.matchedSymbolPositions],
                        id: item.id,
                        reward: item.reward
                    });
                }
                this.matchedSymbolPositions.length = 0;
            }
        })
        return winlines;
    }
}