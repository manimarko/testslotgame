import {Node} from "./Node";

export class ActionNodesBuilder
{
    rootNode = new Node();

    get(path: string): Node {
        return this.createByPathArray(this.rootNode, path.split("."));
    }

    createByPathArray(obj: Node, path: Array<string>): Node {
        let index = 1;
        while (true) {
            if (index >= path.length)
                return obj;
            let childName = path[index];
            const child = obj.getChildByName(childName);
            if (!child) {
                let command = new Node();
                command.name = childName;
                obj.children.push(command);
            }
            index++;
            obj = obj.getChildByName(childName) as Node;
        }
    }
}