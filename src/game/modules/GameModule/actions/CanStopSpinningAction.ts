import {DI} from "../../../../engine/DI";
import {ISpinData, ServerAPI} from "../models/ServerAPI";
import {ResponseParser} from "../models/ResponseParser";
import {ReelsModel} from "../models/ReelsModel";
import {MultiItemsPool} from "../../../../engine/Pool";
import {IAction, Node} from "../../../../engine/command/Node";
import EventEmitter = PIXI.utils.EventEmitter;
import {PlayBtnComponent} from "../components/PlayButtonComponent";

export class CanStopSpinningAction implements IAction
{
    emitter: EventEmitter = DI.Inject(EventEmitter);
    reelModel: ReelsModel = DI.Inject(ReelsModel);
    responseParser: ResponseParser = DI.Inject(ResponseParser);

    async execute()
    {
        this.emitter.emit(PlayBtnComponent.SET_STATE, PlayBtnComponent.ENABLED);
        this.reelModel.canStop(this.responseParser.view);
        const fastStop = this.reelModel.stop.bind(this.reelModel);
        this.emitter.addListener(PlayBtnComponent.START_SPIN, fastStop);
        await this.reelModel.stopPromise;
        this.emitter.removeListener(PlayBtnComponent.START_SPIN, fastStop);
    }
}