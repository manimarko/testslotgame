export class ObservableValue
{
	static simple = v => v.value;
	name: string;
	value: number = 0;
	calculatedValue: number = 0;
	first = true;
	onCalculated = [];
	calculateFunc: Function = ObservableValue.simple;
	dependsValues: Array<ObservableValue> = [];
	bindableUpdate = this.update.bind(this);
	clear() {
		this.onCalculated = [];
		this.dependsValues = [];
	}
	addDependency(v: ObservableValue) {
		if (this.dependsValues.indexOf(v) > -1) {
			return;
		}
		v.onCalculated.push(this.bindableUpdate);
		this.dependsValues.push(v);
	}
	setValue(v: number) {
		if (Math.abs(v - this.value) > 0.001 || this.first) {
			this.first = false;
			this.value = v;
			this.update();
		}
	}
	update() {
		this.calculatedValue = this.calculateFunc.call(null, this, ...this.dependsValues);
		this.onCalculated.forEach(v => v());
	}
	subscribe(bindUpdate: any) {
		const index = this.onCalculated.indexOf(bindUpdate);
		if (index === -1) {
			this.onCalculated.push(bindUpdate);
		}
	}
	unsubscribe(bindUpdate: any): any {
		const index = this.onCalculated.indexOf(bindUpdate);
		if (index > -1) {
			this.onCalculated.splice(index, 1);
		}
	}
}
