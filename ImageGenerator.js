const path = require('path'), fs = require('fs'), fse = require('fs-extra');
const {createCanvas} = require('canvas');

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function saveObject(path, data) {
    fs.writeFileSync(path, JSON.stringify(data, null, '\t'));
}

class ImageGenerator {
    static getSize(fillArea, aspect) {
        fillArea /= 100;
        let square = 1280 * 720 * fillArea;
        let y = Math.sqrt(square / aspect);
        let x = square / y;
        return {x: x, y: y};
    }

    static setImageData(imgData, valueData) {
        let valueDataArr = valueData.split(":");
        let dataInfo = valueDataArr[1].split(",");
        if (valueDataArr[0] == "size") {
            imgData.w = parseFloat(dataInfo[0]);
            imgData.h = parseFloat(dataInfo[1]);
        }
        if (valueDataArr[0] == "area") {
            let size = this.getSize(parseFloat(dataInfo[0]), parseFloat(dataInfo[1]));
            imgData.w = size.x;
            imgData.h = size.y;
        }
        if (valueDataArr[0] == "color") {
            imgData.color = dataInfo[0];
        }
    }

    static parseImageData(imagesData, name) {
        let info = {name: name, w: 0, h: 0, color: 0xFF6633};

        let arrayInfo = imagesData.split('|');

        for (let i = 0; i < arrayInfo.length; i++) {
            this.setImageData(info, arrayInfo[i]);
        }

        return info;
    }

    static createMockTexture(w, h, c, name, lineSize = 1) {
        const textureName = './resources/images/' + name + '.png';

        if (!fs.existsSync(textureName)) {
            const canvas = createCanvas(w, h);
            const ctx = canvas.getContext('2d');
            ctx.fillStyle = c;
            ctx.fillRect(0, 0, w, h);

            ctx.strokeStyle = "#FFFFFF";
            ctx.strokeRect(0, 0, w, h);

            const out = fs.createWriteStream(textureName);
            const stream = canvas.createPNGStream();
            stream.pipe(out);
            out.on('finish', () => console.log(`The PNG file ${name} was created.`));
        }
    }

    static saveImage(info) {
        this.createMockTexture(info.w, info.h, info.color, info.name);
    }

    static generate(data) {
        let parsedData = {};
        for (const key in data) {
            parsedData[key] = this.parseImageData(data[key], key)
            ImageGenerator.saveImage(parsedData[key]);
        }
        return parsedData;
    }
}

const sourceFolder = './resources/';
const destFolder = './dist/resources/';

const uniqPictIds = {};
const imagesInPrefabs = {};
const prefabsInPrefabs = {};
let imagesData = {};
const prefabsData = JSON.parse(fs.readFileSync(destFolder + 'prefabs.json', "utf-8"));

Object.values(prefabsData).forEach(prefabCommands =>
    prefabCommands.forEach(cmd => {
        let pictId = "";

        if (Reflect.has(cmd.compValues, "pictId")) {
            pictId = cmd.compValues.pictId;
        }

        if (Reflect.has(cmd.compValues, "sPictId")) {
            pictId = cmd.compValues.sPictId;
        }

        if (pictId != "") {
            if (!imagesInPrefabs[cmd.prefabName]) {
                imagesInPrefabs[cmd.prefabName] = {};
            }
            imagesInPrefabs[cmd.prefabName][pictId] = 1;
            uniqPictIds[pictId] = 1;
        }

        if (cmd.pointers && cmd.pointers.length > 0) {
            if (!prefabsInPrefabs[cmd.prefabName]) {
                prefabsInPrefabs[cmd.prefabName] = {dependencies: {}};
            }
            cmd.pointers.forEach(p => {
                if (!prefabsInPrefabs[p]) {
                    prefabsInPrefabs[p] = {dependencies: {}};
                }
                prefabsInPrefabs[cmd.prefabName].dependencies[p] = prefabsInPrefabs[p];
            });
        }

    })
);

function getAll(obj, currPrefabName, imagesData) {
    Object.keys(obj.dependencies).forEach(depName => {
        imagesData[currPrefabName] = {...imagesData[currPrefabName], ...imagesInPrefabs[depName]};
        getAll(obj.dependencies[depName], currPrefabName, imagesData);
    });
}

let imagesFilePath = sourceFolder + 'images.json';

if (fs.existsSync(imagesFilePath)) {
    imagesData = JSON.parse(fs.readFileSync(imagesFilePath, "utf-8"));
}

for (const pictId in uniqPictIds) {
    if (uniqPictIds.hasOwnProperty(pictId)) {
        if (!Reflect.has(imagesData, pictId)) {
            imagesData[pictId] = "size:50,50|color:#FF6633";
        }
    }
}

fs.mkdirSync(destFolder + "/images", {recursive: true});

saveObject(imagesFilePath, imagesData);

ImageGenerator.generate(imagesData);

let data = JSON.parse(JSON.stringify(imagesInPrefabs));
Object.keys(prefabsInPrefabs).forEach(key => {
    getAll(prefabsInPrefabs[key], key, data);
});

saveObject(destFolder + "images.json", data);
saveObject(destFolder + "images_raw.json", imagesInPrefabs);
fse.copySync('./resources/images', destFolder + '/images', {overwrite: true})