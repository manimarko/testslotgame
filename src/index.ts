import {DebugModule} from "./game/modules/DebugModule/DebugModule";
import {EngineModule} from "./game/modules/EngineModule/EngineModule";
import {GameModule} from "./game/modules/GameModule/GameModule";
import {GameStarter} from "./engine/GameStarter";

const starter = new GameStarter();
starter.setModules([EngineModule, GameModule, DebugModule]);
starter.execute();


